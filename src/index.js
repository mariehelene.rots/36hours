var express = require("express");
var app = express();
var server = require("http").Server(app);
var io = require("socket.io").listen(server);

app.get("/index", function(req, res) {
  res.sendFile(__dirname + "/index.html");
});

app.use(express.static(__dirname + "/assets"));

app.get("/index", function(req, res) {
  res.sendFile(__dirname + "/index.html");
});

app.get("/control", function(req, res) {
  res.sendFile(__dirname + "/control.html");
});

app.get("/main", function(req, res) {
  res.sendFile(__dirname + "/main.css");
});

app.get("/fragmentshader.glsl.js", function(req, res) {
  res.sendFile(__dirname + "/fragmentshader.glsl.js");
});

app.get("/vertexshader.glsl.js", function(req, res) {
  res.sendFile(__dirname + "/vertexshader.glsl.js");
});

io.on("connection", function(socket) {
  console.log("a user connected");
  socket.on("height", function(value) {
    io.emit("height", value);
  });
});

server.listen(3000, function() {
  console.log("listening on *:3000");
});
